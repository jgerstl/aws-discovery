// app_key and app_id hardcoded from https://tools.adidas-group.com/confluence/pages/viewpage.action?pageId=188551966
// using dev environment

var express = require('express');
var app = express();
var nock = require('nock');
var fs = require('fs');
var capi = require("./capi.js");
var redis = require("redis");
var config = require('./config.js');

// Changing environments
app.get('/env/dev', function(req, res) {
	config.setConfig('DEV', '0122148e', 'f24368e6359d340367ca36a724083453');
	config.logConfig();
	res.send(config.getConfig());
})

app.get('/env/qa', function(req, res) {
	config.setConfig('QA', 'QA_ID', 'QA_KEY');
	config.logConfig();
	res.send(config.getConfig());
})

app.get('/env/prod', function(req, res) {
	config.setConfig('PROD', 'PROD_ID', 'PROD_KEY');
	config.logConfig();
	res.send(config.getConfig());
})

// Redis
app.get('/testRedis', function(req, res) {

})

// Adidas API routes
// Default
app.get('/', function(req, res) {
	res.send('Orchestration layer');
})

// TEST DELETE
app.get('/test', function(req, res) {
	res.send('This is a test page for the data');
})

// Get all mastheads
app.get('/mastheads', function(req, res) {
	var secrets = config.getConfig();
	var options = {
	    host: 'dev.brand.api.adidas.com',
	    path: '/capi/contents/mastheads',
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json',
	        'app_key': secrets.app_key,
	        'app_id': secrets.app_id
	    }
	}

	capi.getCapi(options, function(statusCode, result) {
		res.send(JSON.stringify(result));
	})
})

// Get all media content
app.get('/media', function(req, res) {
	var secrets = config.getConfig();
	var options = {
	    host: 'dev.brand.api.adidas.com',
	    path: '/capi/contents/media-galleries',
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json',
	        'app_key': secrets.app_key,
	        'app_id': secrets.app_id
	    }
	}

	capi.getCapi(options, function(statusCode, result) {
    	res.send(JSON.stringify(result));
	})
})

// Get the navigation tree
app.get('/navigate', function(req, res) {
	var secrets = config.getConfig();
	console.log('hit navigate');
	var options = {
	    host: 'dev.brand.api.adidas.com',
	    path: '/navigation/US/en',
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json',
	        'app_key': secrets.app_key,
	        'app_id': secrets.app_id
	    }
	}

	capi.getCapi(options, function(statusCode, result) {
    	res.send(JSON.stringify(result));
	})
})

// Get all markets
app.get('/markets', function(req, res) {
	var secrets = config.getConfig();
	var options = {
	    host: 'dev.brand.api.adidas.com',
	    path: '/markets?limit=20&offset=0',
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json',
	        'app_key': secrets.app_key,
	        'app_id': secrets.app_id
	    }
	}

	capi.getCapi(options, function(statusCode, result) {
    	res.send(JSON.stringify(result));
	})
})

// Get a market
app.get('/market', function(req, res) {
	var secrets = config.getConfig();
	var options = {
	    host: 'dev.brand.api.adidas.com',
	    path: '/markets/US',
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json',
	        'app_key': secrets.app_key,
	        'app_id': secrets.app_id
	    }
	}

	capi.getCapi(options, function(statusCode, result) {
    	res.send(JSON.stringify(result));
	})
})

// Mock callouts
var mockOptions = {allowUnmocked: true};
var navigationTree = nock('https://dev.brand.api.adidas.com', mockOptions)
.persist()
.get('/navigation/US/en')
.reply('200', function(uri, requestBody) {
	var data = fs.readFileSync('dummy_data/navigation_tree.json', 'utf8');
	return data;
})

var allMarkets = nock('https://dev.brand.api.adidas.com', mockOptions)
.persist()
.get('/markets?limit=20&offset=0')
.reply('200', function(uri, requestBody) {
	var data = fs.readFileSync('dummy_data/all_markets.json', 'utf8');
	return data;
})

var market = nock('https://dev.brand.api.adidas.com', mockOptions)
.persist()
.get('/markets/US')
.reply('200', function(uri, requestBody) {
	var data = fs.readFileSync('dummy_data/market.json', 'utf8');
	return data;
})

// Start the server locally
var server = app.listen(8081, function () {

   var host = server.address().address
   var port = server.address().port
   console.log("Server up at http://%s:%s", host, port)
})
