var config = {};
config.env = 'DEV'
config.app_id = '';
config.app_key = '';

exports.getConfig = function() {
	return config;
}

exports.setConfig = function(env, app_id, app_key) {
	config.env = env;
	config.app_id = app_id;
	config.app_key = app_key;
}

exports.logConfig = function() {
	console.log(config.env);
	console.log(config.app_id);
	console.log(config.app_key);
}
