var https = require("https");

exports.getCapi = function(options, callback) {
    var req = https.request(options, function(res) {
        var output = '';
        res.setEncoding('utf8');

        res.on('data', function (data) {
            output += data;
        });

        res.on('end', function() {
            try {
                // console.log('start.parse output');
                var obj = JSON.parse(output);

                // console.log('end. parse output.'+JSON.stringify(obj));

                console.log('end. parse output.'+JSON.stringify(obj, null, 4) );

            } catch(error) {
                console.log('ERROR OCCURED: ');
                var obj = {'error': output};
            }
            callback(res.statusCode, obj);
        });
    });

    req.on('error', function(err) {
        console.log('error occured: ');
        callback(res.statusCode, {'error': err.message});
    });

    req.end();
};
